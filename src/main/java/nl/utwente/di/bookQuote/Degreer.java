package nl.utwente.di.bookQuote;

public class Degreer {
    public double getBookPrice(String c) {
        return Double.parseDouble(c) * 1.8 + 32;
    }
}
